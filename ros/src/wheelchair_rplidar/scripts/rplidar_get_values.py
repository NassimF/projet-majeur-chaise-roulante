#!/usr/bin/env python

import rospy
from sensor_msgs.msg import LaserScan, PointCloud2
from laser_assembler.srv import *

#You may have to change the Rate values
#At 1Hz the refreashing it too slow, at 10Hz data are not published, 7Hz may be the optimsed value
#/scan and /scan_filtered are published at 14Hz

#One solution could be to publish resp.cloud in a loop to Rate r.sleep() is finished

class rplidar_get_values_node():

    def __init__(self):
        rospy.init_node("rplidar_get_values_node")
        rospy.wait_for_service("assemble_scans2") #Service from laser_scan_assemble.cpp, create a pointcloud2 wither LaserScan values

        self.sub = rospy.Subscriber('/scan', LaserScan, self.pointcloud_callback)
        self.scan_filtered_pub = rospy.Publisher('/scan_filtered', LaserScan, queue_size=10)
        self.rplidar_pointcloud_pub = rospy.Publisher('/rplidar_pointcloud', PointCloud2, queue_size=10)

        self.assemble_scans = rospy.ServiceProxy('assemble_scans2', AssembleScans2)

        self.scan_filtered = LaserScan()
        self.pointcloud_merge = PointCloud2()
        self.current_time = rospy.get_rostime()
        self.previous_time = self.current_time

    def pointcloud_callback(self, scan):
        self.scan_filtered = scan #Copy scan in object variable
        self.laserscan_filter()

    def laserscan_filter(self):
        self.scan_filtered.ranges = list(self.scan_filtered.ranges) #A list is requiered to modify values of ranges[]
        index_max = len(self.scan_filtered.ranges) - 1

        if(index_max == 359):
            if (self.scan_filtered.header.frame_id == "rplidar_right"): #Process for rplidar_right
                #Loops for removing useless data, ones behind the chair and at legs place
                for i in range (0, 34): #Changing those values change the filtered ranges
                    (self.scan_filtered.ranges[i]) = float('inf') #'inf' is nomenclature for None
                for i in range (209, 359):
                    (self.scan_filtered.ranges[i]) = float('inf')

            if (self.scan_filtered.header.frame_id == "rplidar_left"): #Process for rplidar_left
                #Loops for removing useless data, ones behind the chair and at legs place
                for i in range (0, 150):
                    (self.scan_filtered.ranges[i]) = float('inf')
                for i in range (325, 359):
                    (self.scan_filtered.ranges[i]) = float('inf')

        self.scan_filtered_pub.publish(self.scan_filtered)

    def pointcloud_merge_publisher(self):
        self.current_time = rospy.get_rostime()

        self.pointcloud_merge = self.assemble_scans(self.previous_time, self.current_time)

        self.previous_time = self.current_time

if __name__ == "__main__":
    object_instance = rplidar_get_values_node()
    r = rospy.Rate(5) #Rate of merge topic publisher

    while not rospy.is_shutdown():
        object_instance.pointcloud_merge_publisher()
        object_instance.rplidar_pointcloud_pub.publish(object_instance.pointcloud_merge.cloud)
        r.sleep()

    rospy.spin()
