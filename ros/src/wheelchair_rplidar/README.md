# wheelchair_rplidar package

## Feature Summary
Based on rplidar_ros package from robopeak, this package goal is to filter rplidar (left and right) values to keep only the usefull ones for navigating and merge them on 3D pointcloud.
LaserScan values are necesary for a mobile base moving in an environment.

## Benefits
Install all required packages by source.

Check that you have already run ./wheelchair_description/create_udev_rules.sh

Plug the rplidars once checked.

You can check if rplidar_right and rplidar_left existe with ls /dev command. If not, you may have to change the wheelchair_usb.rules.

Launch wheelchair_rplidar.launch.

Topics :
/scan : LaserScan from rplidar
/scan_filtered : LaserScan from rplidar filtered for showing only interesting data
/rplidar_pointcloud : PointCloud2 from laser_assembler merge

## Requirements
Packages requiered :

- wheelchair_rplidar
- wheelchair_description

- /sdk and /src folder from rplidar_ros package, see below for the link
- laser_assembler
- geometry for Tf

Install them by source.

## Links / References
- [Github](https://github.com/robopeak/rplidar_ros) official rplidar_ros package


Author : @Alexis @Michel, @Jean-Baptiste @Rambaud, @Nicolas @Montvernay, @Sébastien @Altounian
