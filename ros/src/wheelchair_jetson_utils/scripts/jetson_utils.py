#!/usr/bin/env python
# -*- coding:utf-8 -*-
import subprocess
import rospy
from std_msgs.msg import Bool, String
import time

class JetsonUtils:

    def __init__(self):
        rospy.init_node('jetson_utils')
        rospy.Subscriber("/jetson_utils/shell_cmd", String, self.received_command)
        self.pub_shell_output = rospy.Publisher("jetson_utils/shell_output", String, queue_size=10)
        rospy.spin()

    def received_command(self, cmd):
        
        if cmd.data == "shutdown":
            try:
                proc = subprocess.Popen('echo "astro4student" | sudo -S shutdown', shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
                out, err = proc.communicate()
                if proc.returncode != 0:
                    shell_resp = "--> The command returned an error <-- <br>" + self.htmlshape_shell_output(err)
                else:
                    shell_resp = "--> Command executed <-- <br>" + "Shuting down... May take 1 or 2 minutes!<br>"+ self.htmlshape_shell_output(out)
                time.sleep(1)
                self.pub_shell_output.publish(shell_resp)
            except Exception as e:
                self.pub_shell_output.publish(str(e))

        elif cmd.data == "reboot":
            try :
                proc = subprocess.Popen('echo "astro4student" | sudo -S reboot', shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
                out, err = proc.communicate()
                if proc.returncode != 0:
                    shell_resp = "--> The command returned an error <-- <br>" + self.htmlshape_shell_output(err)
                else:
                    shell_resp = "--> Command executed <-- <br>" + "Rebooting... May take 1 or 2 minutes!<br>"+ self.htmlshape_shell_output(out)
                time.sleep(1)
                self.pub_shell_output.publish(shell_resp)
            except Exception as e:
                self.pub_shell_output.publish(str(e))
        else:
            try :
                proc = subprocess.Popen(cmd.data, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
                out, err = proc.communicate()
                if proc.returncode != 0:
                    shell_resp = "--> The command returned an error <-- <br>" + self.htmlshape_shell_output(err)
                else:
                    shell_resp = "--> Command executed <-- <br>" + self.htmlshape_shell_output(out)
                time.sleep(1)
                self.pub_shell_output.publish(shell_resp)
            except Exception as e:
                self.pub_shell_output.publish(str(e))
    
    
    def htmlshape_shell_output(self, string):
        htmlshaped_string = ""
        for val in string.split("\n"):
            htmlshaped_string += val + "<br>"
        return htmlshaped_string


if __name__ == "__main__":
    while not rospy.is_shutdown():
        try:
            jetson_interact = JetsonUtils()
        except rospy.ROSInterruptException:
            pass

