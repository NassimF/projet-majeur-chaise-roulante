#!/usr/bin/env python

import rospy
import serial
from rospy_message_converter import json_message_converter
from geometry_msgs.msg import Twist
from wheelchair_website.msg import PidUpdate


class STM32Com():

  def __init__(self):
    rospy.init_node('stm32_com_node')
    #serial_port = "/dev/stm_serial" #Read STM32 port thanks to wheelchair_usb.rules
    serial_port = "/dev/serial/by-path/platform-3610000.xhci-usb-0:4.4:1.0-port0"
    self.ser = serial.Serial(serial_port, 115200) #Open a serial com
    print ">>>>>>>>>>STM32 connected to serial port {}".format(serial_port)
    rospy.Subscriber('/cmd_vel', Twist, self.cmd_callback)
    #rospy.Subscriber('/my_cmd_vel', Twist, self.cmd_callback)
    rospy.Subscriber('/website_pub_settings', PidUpdate, self.pid_callback)
    self.isBusy = False


  def cmd_callback(self, cmd_msg):
    while self.isBusy:
        pass
    self.isBusy = True
    self.cmd_json = json_message_converter.convert_ros_message_to_json(cmd_msg) #Convert cmd_vel into json
    self.ser.write(self.cmd_json + "\r\n") #Write the json on serial
    self.isBusy = False
    print(self.cmd_json)

  def pid_callback(self, cmd_msg):
    while self.isBusy:
      pass
    self.isBusy = True
    self.pid_json = json_message_converter.convert_ros_message_to_json(cmd_msg) #Convert cmd_vel into json
    self.ser.write(self.pid_json + "\r\n") #Write the json on serial
    self.isBusy = False
    print(self.pid_json)

if __name__ == '__main__':
  try:
    STM32Com()
    rospy.spin()

  except rospy.ROSInterruptException:
    print("Erreur")
    pass
