#!/usr/bin/env python

import rospy
import serial
from rospy_message_converter import json_message_converter
from nav_msgs.msg import Odometry

import numpy as np

class OdomStatePublisher():

  def __init__(self):
    rospy.init_node('odom_state_publisher')
    #serial_port = "/dev/stm_serial" #Read STM32 port thanks to wheelchair_usb.rules
    serial_port = "/dev/serial/by-path/platform-3610000.xhci-usb-0:4.4:1.0-port0"
    self.ser = serial.Serial(serial_port, 115200) #Open a serial com
    print ">>>>>>>>>>STM32 connected to serial port {}".format(serial_port)
    self.pub_odom = rospy.Publisher('/odom', Odometry, queue_size=10)

  def start(self):
    rate = rospy.Rate(10) # USB reading frequency
    while not rospy.is_shutdown():
      odom_json = self.ser.readline()
      if odom_json.startswith('{'):
        odom_msg = json_message_converter.convert_json_to_ros_message('nav_msgs/Odometry', odom_json) #Convert inot ros_message
        odom_msg.header.stamp = rospy.Time.now()
        odom_msg.pose.covariance = [0.01, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.01, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.01, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.01, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.01, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.01]
        odom_msg.twist.covariance = [0.01, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.01, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.01, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.01, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.01, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.01]
        self.pub_odom.publish(odom_msg)
      rate.sleep()

if __name__ == '__main__':
  try:
    odom_state_publisher = OdomStatePublisher()
    odom_state_publisher.start()
  except rospy.ROSInterruptException:
    pass
