#!/usr/bin/env python

import rospy
from geometry_msgs.msg import PoseWithCovarianceStamped, PoseWithCovariance, Point, Quaternion
from nav_msgs.msg import Odometry
import tf

class tf_broadcaster():

    def __init__(self):

        rospy.init_node('tf_broadcaster')
        rospy.Subscriber('/odom', Odometry, self.odom_callback)
        rospy.sleep(6) #Wait for every node to be lunched
        self.odom_broadcaster = tf.TransformBroadcaster()
        self.rate = rospy.Rate(5) #Rate of tf odom publishing
        self.odom_info = None

    def odom_callback(self,msg):
        self.odom_info = msg

    def tf_transforms(self):
        if self.odom_info != None: #Check if we have received odom info
            current_time = rospy.Time.now()
            #send tf between odom and base_link
            self.odom_broadcaster.sendTransform(
            (self.odom_info.pose.pose.position.x, self.odom_info.pose.pose.position.y, self.odom_info.pose.pose.position.z),
            (self.odom_info.pose.pose.orientation.x, self.odom_info.pose.pose.orientation.y, self.odom_info.pose.pose.orientation.z, self.odom_info.pose.pose.orientation.w),
            current_time,
            "base_link",
            "odom"
            )

if __name__ == '__main__':
    try:
        tf_wheelchair = tf_broadcaster()

        while not rospy.is_shutdown():
            tf_wheelchair.tf_transforms()
            tf_wheelchair.rate.sleep()

        rospy.spin()

    except rospy.ROSInterruptException:
        pass
