#!/usr/bin/env python
# -*- coding:utf-8 -*-

import rospy
from geometry_msgs.msg import PoseStamped, PoseWithCovarianceStamped
from wheelchair_website.msg import PID, PidUpdate

from rospy_message_converter import json_message_converter
import json

from flask import Flask, render_template, redirect, url_for, request
from flask_cors import CORS, cross_origin

from std_msgs.msg import String

from urllib import unquote_plus

app = Flask(__name__)
CORS(app)


################################################# FUNCTIONS #################################################

# transformation between x value from 'MAP' and the real x value from the map (thirfloor.pgm)
def transform_x(x_val):
    return float(x_val)*1397/254 * resolution

# transformation between y value from 'MAP' and the real y value from the map (thirfloor.pgm)
def transform_y(y_val):
    return (325-float(y_val))*2274/414 * resolution

################################################# DECLARATIONS #################################################

resolution = 0.025

pid_values = {
    "linear": { "Kp": 0.2, "Ki": 0.005, "Kd": 0.01, "error_stop": 0.6, "wind_up": 0.05},
    "angular": { "Kp": 0.5, "Ki": 0.005, "Kd": 0.01, "error_stop": 0.2, "wind_up": 0.02}
}

rooms = [
    {"id": "robotic", "x": transform_x(48), "y": transform_y(274), "w": 1.0 },
    {"id": "corridor", "x": transform_x(81), "y": transform_y(83), "w": 1.0 }
]

initial_poses = [
    {"id": "red", "x": 14.73, "y": 45.12, "z": -0.77, "w": 0.64 },
    {"id": "blue", "x": 33.55, "y":7.23, "z":0.994 , "w": 0.11 },
    {"id": "green", "x": 5.75, "y":11.69,"z":-0.11 , "w": 0.994 },
    {"id": "pink", "x": 10.66, "y":12.62,"z":0.65 , "w": 0.76 }
]

################################################# WEB PAGE: HOME #################################################

@app.route('/')
@cross_origin()
def index():
    return render_template('index.html')

################################################# WEB PAGE: CONTACT US #################################################

@app.route('/contact')
@cross_origin()
def contact():
    return render_template('contact.html')

################################################# WEB PAGE: ABOUT US #################################################

@app.route('/about')
@cross_origin()
def about():
    return render_template('about.html')

################################################# WEB PAGE: SET AN INITIAL POSITION #################################################

@app.route('/set_initial_position')
@cross_origin()
def set_initial_position():
    return render_template('set_initial_position.html')

@app.route('/send_initial_position/<pose_id>')
@cross_origin()
def send_initial_pose(pose_id):
    initial_pose = [r for r in initial_poses if r['id'] == pose_id][0]
    try:
        pose_init = PoseWithCovarianceStamped()
        pose_init.pose.pose.position.x = initial_pose["x"]
        pose_init.pose.pose.position.y = initial_pose["y"]
        pose_init.pose.pose.orientation.z = initial_pose["z"]
        pose_init.pose.pose.orientation.w = initial_pose["w"]
        pose_init.header.stamp =  rospy.Time.now()
        pose_init.header.frame_id = "map"
        # Publish initial position, topic: initialpose
        pub_initial.publish(pose_init)
        return redirect(url_for("set_initial_position", alert_type='success', x_initial_position=initial_pose["x"], y_initial_position=initial_pose["y"]))
    except rospy.ROSInterruptException:
        return redirect(url_for("set_initial_position", alert_type='error', x_initial_position=initial_pose["x"], y_initial_position=initial_pose["y"]))

################################################# WEB PAGE: SET A GOAL THROUGH THE MAP #################################################

@app.route('/set_goal_through_map')
@cross_origin()
def set_goal_through_map():
    return render_template('set_goal_through_map.html')

@app.route('/go_to_xy/<x>/<y>')
@cross_origin()
def go_to_xy(x, y):
    try:
        goal_map = PoseStamped()
        goal_map.pose.position.x = float(x)
        goal_map.pose.position.y = float(y)
        goal_map.pose.orientation.w =  1.0
        goal_map.header.stamp =  rospy.Time.now()
        goal_map.header.frame_id = "map"
        # Publish goal, topic: move_base_simple/goal
        pub_map.publish(goal_map)
        return redirect(url_for("set_goal_through_map", alert_type='success', x_goal_position=float(x), y_goal_position=float(y)))
    except rospy.ROSInterruptException:
        return redirect(url_for("set_goal_through_map", alert_type='error', x_goal_position=float(x), y_goal_position=float(y)))

################################################# WEB PAGE: SET A GOAL THROUGH A ROOM NAME #################################################

@app.route('/set_goal_through_room')
@cross_origin()
def set_goal_through_room():
    return render_template('set_goal_through_room.html')

@app.route('/go_to_room/<room_id>')
@cross_origin()
def go_to_room(room_id):
    room = [r for r in rooms if r['id'] == room_id][0]
    try:
        goal = PoseStamped()
        goal.pose.position.x = room['x']
        goal.pose.position.y =  room['y']
        goal.pose.orientation.w =  room['w']
        goal.header.stamp =  rospy.Time.now()
        goal.header.frame_id = "map"
        # Publish goal, topic: move_base_simple/goal
        pub_room.publish(goal)
        return redirect(url_for("set_goal_through_room", alert_type='success', x_goal_position=room['x'], y_goal_position=room['y']))
    except rospy.ROSInterruptException:
        return redirect(url_for("set_goal_through_room", alert_type='error', x_goal_position=room['x'], y_goal_position=room['y']))

################################################# WEB PAGE: MAP THE ENVIRONMENT #################################################

@app.route('/mapping')
@cross_origin()
def mapping():
    return render_template('mapping.html')

################################################# WEB PAGE: Interact with the JETSON through shell command #################################################

@app.route('/interact_w_jetson')
@cross_origin()
def interact_w_jetson():
    return render_template('interact_w_jetson.html')

@app.route('/execute_command/<command>')
@cross_origin()
def exceute_shell_cmd(command):
    if "SLASH" in command:
        command = command.replace("SLASH" ,"%2F")
    cmd = unquote_plus(command)
    try:
        pub_jetson_utils.publish(cmd)
        return redirect(url_for("interact_w_jetson", alert_type='success'))
    except rospy.ROSInterruptException:
        return redirect(url_for("interact_w_jetson", alert_type='error'))

    
################################################# WEB PAGE: SET PID PARAMETERS #################################################

@app.route('/pid_settings')
@cross_origin()
def pid_settings():
    return render_template('pid_settings.html')

@app.route('/send_pid_settings', methods=["POST"])
@cross_origin()
def send_pid_settings():
    if request.method == 'POST':
        for key in ['Kp', 'Ki', 'Kd', 'error_stop', 'wind_up']:
            pid_values['linear'][key] = float(request.form['linear_' + key].encode('utf-8'))
            pid_values['angular'][key] = float(request.form['angular_' + key].encode('utf-8'))
        try:
            pid_settings = json_message_converter.convert_json_to_ros_message('wheelchair_website/PidUpdate', json.dumps(pid_values))
            # Publish PID param, topic: website_pub_settings
            pub_settings.publish(pid_settings)
            return redirect(url_for("pid_settings", alert_type='success'))
        except rospy.ROSInterruptException:
            return redirect(url_for("pid_settings", alert_type='error'))
    else:
        return redirect(url_for("pid_settings", alert_type='error'))


if __name__ == '__main__':
    try:
        # Create ros node
        rospy.init_node('wheelchair_website_node')
        # Publishers declarations
        pub_settings = rospy.Publisher('website_pub_settings', PidUpdate, queue_size=100)
        pub_room = rospy.Publisher('move_base_simple/goal', PoseStamped, queue_size=10)
        pub_map = rospy.Publisher('move_base_simple/goal', PoseStamped, queue_size=10)
        pub_initial = rospy.Publisher('initialpose', PoseWithCovarianceStamped, queue_size=10)
        pub_jetson_utils = rospy.Publisher('jetson_utils/shell_cmd', String, queue_size=10)
        # Launch flask application at a specific adress and port
        app.run(host='0.0.0.0', port=5000, debug=True)
        #app.run(host='127.0.0.1', port=5000, debug=True)
        rospy.spin()
    except rospy.ROSInterruptException:
        pass
