# Website



## About the project

### Context

This package aims to create a website allowing the user to control the wheelchair.

In a known map, send to the wheelchair an initial position, a goal coordinates and change PID parameters thanks to a website.
The wheelchair will move autonomously to the destination.

Thanks to the application, the user can either :
* Go to a specific room with feedback
* Go to a specific map location with feedback
* Set an initial position with feedback
* Modify PID's parameters
* Send linux commands to the Jetson with feedback
* Map a new environment (TO DO)
* Use different maps for his navigation (TO DO)

### Visuals

Pictures of the website are available in the `docs` folder.

Home page
![](docs/home.png)

About page
![](docs/about.png)

Contact page
![](docs/contact.png)

Initial position page
![](docs/initial_position.png)

Goal through map page
![](docs/goal_through_map.png)

Goal through room page
![](docs/goal_through_room.png)

Mapping page
![](docs/mapping.png)

PID param page
![](docs/pid_param.png)

Jetson's shell command page
![](docs/send_jetson_commands.png)



## Installation

### Hardware

1. A client device: Tablet/Smartphone/PC
1. A server device: Jetson Xavier AJX
1. A wifi connection: name=`wheelchair_wifi`, pwd=`wheelchaircpe`, IP=`192.168.1.12:5000`

### Software

List of software versions:

1. ROS on Ubuntu MATE

List of major frameworks/libraries:

1. rospy
1. std_msgs
1. geometry_msgs
1. flask
1. flask-cors
1. [rospy_message_converter](https://github.com/uos/rospy_message_converter)
1. [rosbridge_server](http://wiki.ros.org/rosbridge_server)
1. json



## Usage

### Process

In a terminal:
1. `pip install flask==1.1.4`
1. `pip install flask-cors==3.0.10`
1. `export FLASK_APP=wheelchair_website_node`
1. `export FLASK_ENV=development`
1. `cd [ros workspace directory]`
1. `catkin_make`
1. `source devel/setup.bash`
1. `roslaunch wheelchair_website website.launch`

In a Tablet/Smartphone/PC:
1. Connect to the wifi, name=`wheelchair_wifi`, pwd=`wheelchaircpe`
1. Open a browser, Mozilla, Chrome, Safari, ...
1. Browse `http://192.168.1.12:5000/`

### Features

This application can publish messages on 4 topics:
1. website_pub_settings
1. initialpose
1. move_base_simple/goal
1. jetson_utils/shell_output

This application subscribes to 1 topics:
1. /jetson_utils/shell_cmd

2 messages created:
1. PID.msg
1. PidUpdate.msg

### Content

1. Home : Contains a description of the wheelchair project
1. Initial positition : Set the initial position of the wheelchair
1. Goal through map : Display a map. The user can select a location and send a command to move the chair to the selected location.
1. Goal through rooms : The user can select a room and send a command to move the chair to the selected room.
1. Mapping : 2 possibilites, the user can either create a new map (mapping of the environment) or load an old map to navigate into (TO DO)
1. PID settings : Allows to modify all the parameters of the PID (motors control)
1. Jetson shell commands: Allows to send linux command to the Jetson

### Changing IP address

In case the hostpot changes its IP address, you may need to change the line 172 in the file `interact_w_jetson.html` according to the new IP of the hotspot but the port is static.

Please refers to [rosbridge_server](http://wiki.ros.org/rosbridge_server) documentation



## Roadmap

Ideas for future releases:
1. Use the website in localhost and connect the tablet to the Jetson through a cable
1. Uninstall flask-cors and stop using it
1. Upgrade the jetson shell command page



## Sources

1. [Flask](https://flask.palletsprojects.com/en/2.0.x/tutorial/)
1. [Bootstrap](https://getbootstrap.com/docs/4.6/getting-started/introduction/)
