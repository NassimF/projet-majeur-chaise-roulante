# wheelchair_description package

## Feature Summary
This package goal is to describe the wheelchair and it contains all useful files for using the wheelchair.
This package contains the urdf of the wheelchair, the scripts for creating usb connections rules, a script for launching the remote mode for debug.  
One launch is accessible for checking the urdf file. Lastly there are the rviz config for checking the good behaviour of the wheelchair.

## Benefits

### For changing the urdf :
Implemente your modifications and launch rviz.launch to see them.

### For creating usb connections rules :
Execute ./create_udev_rules.sh which is in /scripts.

Plug the usb ports.

### For changing the usb connections rules :
 You may have to change the usb port : KERNELS=="1-1.2*" for the usb port or ATTRS{idVendor}=="10c4" or ATTRS{idProduct}=="ea60" for the product by the one told using udevadm info -a -p  $(udevadm info -q path -n /dev/ttyUSBX) with X the number show by ls /dev.

If you have change rules execute ./delate_udev_rules.sh and delate /dev/rplidar_left, /dev/rplidar_right and /dev/stm_serial

Execute ./create_udev_rules.sh which is in /scripts.

### For launching the remote mode :
Connect your computer on wheechair_wifi, launch rviz_remote.sh and observe on rviz.

## Requirements
Packages requiered :

- rviz

Author : @Alexis @Michel, @Jean-Baptiste @Rambaud, @Nicolas @Montvernay, @Sébastien @Altounian
