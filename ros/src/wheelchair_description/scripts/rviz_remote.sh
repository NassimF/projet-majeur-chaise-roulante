# THink to export your own IP as ROS_IP var before launching this script
export ROS_IP=10.42.0.68
export ROS_HOSTNAME=10.42.0.68
export ROS_MASTER_URI=http://10.42.0.1:11311
roscore &
rosrun rviz rviz -d src/wheelchair_description/config/navigation.rviz
