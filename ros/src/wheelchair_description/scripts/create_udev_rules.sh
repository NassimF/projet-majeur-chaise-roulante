#!/bin/bash

echo "remap the device serial port(ttyUSBX) to rplidar_left or rplidar_right"
echo "start copy rplidar.rules to  /etc/udev/rules.d/"
sudo cp `rospack find wheelchair_rplidar`/scripts/rplidar.rules  /etc/udev/rules.d
echo "Restarting udev"
sudo service udev reload
sudo service udev restart
echo "finish "
