#!/usr/bin/env python

import rospy
from geometry_msgs.msg import PoseWithCovarianceStamped, PoseWithCovariance, Point, Quaternion

def publish_pose():
    rospy.init_node('publish_initial_pose')
    pub = rospy.Publisher('/initialpose', PoseWithCovarianceStamped, queue_size=10)
    rospy.sleep(2)

    init_pose = PoseWithCovarianceStamped()
    init_pose.header.frame_id = "map"
    init_pose.header.stamp = rospy.Time.now()
    init_pose.pose.pose.position.x = 10
    init_pose.pose.pose.position.z = 0
    init_pose.pose.pose.position.y = 10

    init_pose.pose.pose.orientation.x = 0
    init_pose.pose.pose.orientation.y = 0
    init_pose.pose.pose.orientation.z = 0
    init_pose.pose.pose.orientation.w = 1

    pub.publish(init_pose)

if __name__ == '__main__':
    try:
        publish_pose()
    except rospy.ROSInterruptException:
        pass
