# wheelchair_navigation package

## Safety :

Someone has to sit down on the wheelchair for the good working of the navigation. For changing this condition, you need to change the current PID parameters.

When someone is sitting down on the wheelchair, his legs have to be in center and close to the chair.

## Feature Summary
This package goal is to implement the navigation stack of ROS. You can check on move_base wiki website.


## Benefits
Amcl package is useful to localize the robot in the map.

Map_server is useful to load a map. If you want to change the default map, you can load it in map_user folder and change the name of loaded file in map_server.launch

Move_base, you can change parameters in yaml folder even if you should not change them.

You can change the initial pose through scripts file. This position will be loaded at each start.


## Requirements
Packages requiered :

- wheelchair_navigation
- amcl
- move_base
- map_server


## Links / References
- [mobe_base wiki](http://wiki.ros.org/move_base)


Author : @Alexis @Michel, @Jean-Baptiste @Rambaud, @Nicolas @Montvernay, @Sébastien @Altounian
