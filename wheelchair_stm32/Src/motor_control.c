/**
  ******************************************************************************
  * @file    Src/motor_control.c
  * @author  Nicolas Montvernay
  * @version V0.1
  * @date    08/01/2019
  * @brief   Control motors PWMs
  ******************************************************************************
  */

#include <math.h>
#include "motor_control.h"
#include "main.h"
#include "pid.h"


extern TIM_HandleTypeDef htim3, htim4;
extern Twist_t twist;
extern Odom_Feedback_t odom_feedback;

PID_t pid_lin = { 0.2F, 0.01F, 0.005F, 0.0F, 0.0F, 0.6F, 0.05F };
PID_t pid_ang = { 0.5F, 0.01F, 0.005F, 0.0F, 0.0F, 0.2F, 0.02F };
uint32_t left_motor_stop_time = 0, right_motor_stop_time = 0;

void Start_Motors() {
	HAL_GPIO_WritePin(MOTOR_RIGHT_RELAY_GPIO, MOTOR_RIGHT_RELAY_PIN, GPIO_PIN_SET); // Activate the motor relay
	HAL_GPIO_WritePin(MOTOR_LEFT_RELAY_GPIO, MOTOR_LEFT_RELAY_PIN, GPIO_PIN_SET); // Activate the motor relay
	left_motor_stop_time = right_motor_stop_time = 0;
}

void Motor_Set_Right_Speed(float speed) {
  int direction = (speed > 0.0F) ? GPIO_PIN_RESET : GPIO_PIN_SET;

  if (fabs(speed) > 0.01F) {
		TIM_OC_InitTypeDef sConfigOC = {0};
		sConfigOC.OCMode = TIM_OCMODE_PWM1;
		sConfigOC.OCPolarity = TIM_OCPOLARITY_HIGH;
		sConfigOC.OCFastMode = TIM_OCFAST_DISABLE;
    sConfigOC.Pulse = fabs(speed)*PWM_PERIOD; // Set PWM

    if(HAL_TIM_PWM_ConfigChannel(&htim3, &sConfigOC, MOTOR_RIGHT_CHANNEL) != HAL_OK) // Set the config
      Error_Handler();

		if(HAL_TIM_Base_Start(&htim3) != HAL_OK || HAL_TIM_PWM_Start(&htim3, MOTOR_RIGHT_CHANNEL) != HAL_OK) // Start the timer
			Error_Handler();

		Start_Motors();
    HAL_GPIO_WritePin(MOTOR_RIGHT_DIR_GPIO, MOTOR_RIGHT_DIR_PIN, (GPIO_PinState)direction); // Set direction (forward/backward)
    HAL_GPIO_WritePin(MOTOR_RIGHT_SLEEP_GPIO, MOTOR_RIGHT_SLEEP_PIN, GPIO_PIN_SET); // Deactivate sleep mode
  }
	else
	{
		if(HAL_TIM_Base_Stop(&htim3) != HAL_OK || HAL_TIM_PWM_Stop(&htim3, MOTOR_RIGHT_CHANNEL) != HAL_OK)
			Error_Handler();

	  HAL_GPIO_WritePin(MOTOR_RIGHT_SLEEP_GPIO, MOTOR_RIGHT_SLEEP_PIN, GPIO_PIN_RESET); // Activate sleep mode
		if (right_motor_stop_time == 0)
			right_motor_stop_time = HAL_GetTick();

  }
}

void Motor_Set_Left_Speed(float speed) {
  int direction = (speed < 0.0F) ? GPIO_PIN_RESET : GPIO_PIN_SET;

	if (fabs(speed) > 0.01F) {
		TIM_OC_InitTypeDef sConfigOC = {0};
		sConfigOC.OCMode = TIM_OCMODE_PWM1;
		sConfigOC.OCPolarity = TIM_OCPOLARITY_HIGH;
		sConfigOC.OCFastMode = TIM_OCFAST_DISABLE;
		sConfigOC.Pulse = fabs(speed)*PWM_PERIOD; // Set PWM

		if(HAL_TIM_PWM_ConfigChannel(&htim4, &sConfigOC, MOTOR_LEFT_CHANNEL) != HAL_OK) // Set the config
			Error_Handler();

		if(HAL_TIM_Base_Start(&htim4) != HAL_OK || HAL_TIM_PWM_Start(&htim4, MOTOR_LEFT_CHANNEL) != HAL_OK) // Start the timer
			Error_Handler();

		Start_Motors();
		HAL_GPIO_WritePin(MOTOR_LEFT_DIR_GPIO, MOTOR_LEFT_DIR_PIN, (GPIO_PinState)direction); // Set direction (forward/backward)
		HAL_GPIO_WritePin(MOTOR_LEFT_SLEEP_GPIO, MOTOR_LEFT_SLEEP_PIN, GPIO_PIN_SET); // Deactivate sleep mode
	}
	else
	{
		if(HAL_TIM_Base_Stop(&htim4) != HAL_OK || HAL_TIM_PWM_Stop(&htim4, MOTOR_LEFT_CHANNEL) != HAL_OK)
			Error_Handler();

		HAL_GPIO_WritePin(MOTOR_LEFT_SLEEP_GPIO, MOTOR_LEFT_SLEEP_PIN, GPIO_PIN_RESET); // Activate sleep mode
		if (left_motor_stop_time == 0)
			left_motor_stop_time = HAL_GetTick();
	}
}

float Get_Linear_Speed(float vx, float vy) {
	float abs_speed = sqrt(vx*vx + vy*vy);
	if (vx < 0.0F)
		return -abs_speed;
	return abs_speed;
}

void Update_Speed(void) {
	float lin_speed = Get_Linear_Speed(twist.linear.x, twist.linear.y);

	float lin_odom = Get_Linear_Speed(odom_feedback.vx, odom_feedback.vy);

	float err_lin = lin_speed - lin_odom;
	float err_ang = twist.angular.z - odom_feedback.vangle;

	if (fabs(err_lin) > pid_lin.error_stop || fabs(err_ang) > pid_ang.error_stop) {
		float com_lin = Execute_PID(&pid_lin, err_lin);
		float com_ang = Execute_PID(&pid_ang, err_ang);

		float speed_motor_right = (com_lin + com_ang);
		Motor_Set_Right_Speed(speed_motor_right);

		float speed_motor_left = (com_lin - com_ang);
		Motor_Set_Left_Speed(speed_motor_left);
	}
}


// Function called periodically to activate motor breaks if time elapsed after
// full stop request is more than STOP_MOTORS_DELAY
void Check_Motors_Breaks(void) {
	uint32_t cur_ms = HAL_GetTick();
	if (left_motor_stop_time != 0 && cur_ms > left_motor_stop_time + STOP_MOTORS_DELAY &&
				right_motor_stop_time != 0 && cur_ms > right_motor_stop_time + STOP_MOTORS_DELAY) {
		HAL_GPIO_WritePin(MOTOR_RIGHT_RELAY_GPIO, MOTOR_RIGHT_RELAY_PIN, GPIO_PIN_RESET); // Deactivate the motor relay
		HAL_GPIO_WritePin(MOTOR_LEFT_RELAY_GPIO, MOTOR_LEFT_RELAY_PIN, GPIO_PIN_RESET); // Deactivate the motor relay
	}
}
