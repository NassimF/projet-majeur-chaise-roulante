#include <math.h>
#include "main.h"
#include "pid.h"


float Execute_PID(PID_t *pid, float error) {
	float p = pid->Kp * error;
	pid->integ_err += error;
	float i = pid->Ki * pid->integ_err;
	float d = pid->Kd * (error - pid->prev_err);
	pid->prev_err = error;

	if(i > pid->wind_up)
		i = pid->wind_up;
	else if(i < -pid->wind_up) 
		i = -pid->wind_up;

	return p + i + d;
}
