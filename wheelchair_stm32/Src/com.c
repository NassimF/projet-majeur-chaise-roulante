/**
  ******************************************************************************
  * @file    Src/ros_com.c
  * @author  Nicolas Montvernay
  * @version V0.1
  * @date    08/01/2019
  * @brief   Communication with ros through JSON messages
  ******************************************************************************
  */


#include <cqrlib.h>
#include <cstdio>
#include <string.h>
#include "main.h"
#include "com.h"
#include "pid.h"
#include "cJSON.h"
#include "json_templates.h"


// Private functions
void ros_odom_msg(cJSON *pose, cJSON *twist);
void ros_pose_msg(float pos_x, float pos_y, float pos_z,
  float ori_x, float ori_y, float ori_z, float ori_w);
void ros_twist_msg(float lin_x, float lin_y, float ang_z);
uint8_t Process_Twist(void);
uint8_t Process_PID_Tuning(void);


extern UART_HandleTypeDef husart6;
extern Twist_t twist;
extern Odom_Feedback_t odom_feedback;
extern PID_t pid_lin, pid_ang;


// Private variables
cJSON *pose_msg, *twist_msg, *odom_msg, *received_json;
char odom_buffer[665];
char json_buffer[512];
uint16_t json_buffer_len = 0;
uint8_t json_buffer_char = 0;


void Com_Init(void) {
	// Parse templates
	pose_msg = cJSON_Parse(pose_template);
	twist_msg = cJSON_Parse(twist_template);
	odom_msg = cJSON_Parse(odom_template);

	// Init UART reception
	if (HAL_UART_Receive_DMA(&husart6, &json_buffer_char, 1) != HAL_OK) {
		Error_Handler();
	}
}

void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart) {
	// Write the received char in the buffer
	json_buffer[json_buffer_len] = (char)json_buffer_char;
	json_buffer_len++;

	// if the last char is a new line (end of the message)
	if (json_buffer_char == '\n') {
		json_buffer[json_buffer_len] = '\0';

		// Parse it and reset the buffer
		received_json = cJSON_Parse(json_buffer);
		json_buffer_len = 0;

		if (received_json != NULL) { // Valid message
			if (Process_Twist() || Process_PID_Tuning()) {
					// Message was received and processed
			}

			// Free memory
			cJSON_Delete(received_json);
		}
	}

	// Restart reading
	if (HAL_UART_Receive_DMA(&husart6, &json_buffer_char, 1) != HAL_OK) {
		Error_Handler();
	}
}

uint8_t Process_PID_Tuning(void) {
	cJSON *linear = cJSON_GetObjectItem(received_json, "linear");
	uint8_t is_pid_msg = cJSON_GetObjectItem(linear, "Kp") != NULL;
	if (!is_pid_msg)  // Check if msg is PID Tuning json
		return 0;

	pid_lin.Kp = cJSON_GetObjectItem(linear, "Kp")->valuedouble;
	pid_lin.Ki = cJSON_GetObjectItem(linear, "Ki")->valuedouble;
	pid_lin.Kd = cJSON_GetObjectItem(linear, "Kd")->valuedouble;
	pid_lin.error_stop = cJSON_GetObjectItem(linear, "error_stop")->valuedouble;
	pid_lin.wind_up = cJSON_GetObjectItem(linear, "wind_up")->valuedouble;

	cJSON *angular = cJSON_GetObjectItem(received_json, "angular");
	pid_ang.Kp = cJSON_GetObjectItem(angular, "Kp")->valuedouble;
	pid_ang.Ki = cJSON_GetObjectItem(angular, "Ki")->valuedouble;
	pid_ang.Kd = cJSON_GetObjectItem(angular, "Kd")->valuedouble;
	pid_ang.error_stop = cJSON_GetObjectItem(angular, "error_stop")->valuedouble;
	pid_ang.wind_up = cJSON_GetObjectItem(angular, "wind_up")->valuedouble;


	return 1;

}

uint8_t Process_Twist(void) {
		cJSON *linear = cJSON_GetObjectItem(received_json, "linear");
		uint8_t is_twist_msg = cJSON_GetObjectItem(linear, "x") != NULL;
		if (!is_twist_msg)  // Check if msg is Twist json
				return 0;

		twist.linear.x = cJSON_GetObjectItem(linear, "x")->valuedouble;
		twist.linear.y = cJSON_GetObjectItem(linear, "y")->valuedouble;

		cJSON *angular = cJSON_GetObjectItem(received_json, "angular");
		twist.angular.z = cJSON_GetObjectItem(angular, "z")->valuedouble;

		return 1;
}

void Send_Odom(void) {
  CQRQuaternion pose_quat;
	double axis[3] = {0.,0.,1.};
  CQRAxis2Quaternion(&pose_quat, axis, odom_feedback.angle);

  ros_pose_msg(odom_feedback.x, odom_feedback.y, 0.0F,
    pose_quat.x, pose_quat.y, pose_quat.z, pose_quat.w);
	ros_twist_msg(odom_feedback.vx, odom_feedback.vy, odom_feedback.vangle);
  ros_odom_msg(pose_msg, twist_msg);

  // Convert json to string and send to uart
	cJSON_PrintPreallocated(odom_msg, odom_buffer, 665, 0);
	int32_t len = sprintf(odom_buffer, "%s\r\n", odom_buffer);
	HAL_UART_Transmit_DMA(&husart6, (uint8_t *)odom_buffer, len);

}

void ros_odom_msg(cJSON *pose, cJSON *twist) {
  // odom -> pose -> pose = <position_msg>
  cJSON_ReplaceItemInObject(cJSON_GetObjectItem(odom_msg, "pose"), "pose", pose);

  // odom -> twist -> twist = <twist_msg>
  cJSON_ReplaceItemInObject(cJSON_GetObjectItem(odom_msg, "twist"), "twist", twist);

}

void ros_pose_msg(float pos_x, float pos_y, float pos_z,
  float ori_x, float ori_y, float ori_z, float ori_w) {

    cJSON *position = cJSON_GetObjectItem(pose_msg, "position");
    cJSON_ReplaceItemInObject(position, "x", cJSON_CreateNumber(pos_x));
    cJSON_ReplaceItemInObject(position, "y", cJSON_CreateNumber(pos_y));
    cJSON_ReplaceItemInObject(position, "z", cJSON_CreateNumber(pos_z));

    cJSON *orientation = cJSON_GetObjectItem(pose_msg, "orientation");
    cJSON_ReplaceItemInObject(orientation, "x", cJSON_CreateNumber(ori_x));
    cJSON_ReplaceItemInObject(orientation, "y", cJSON_CreateNumber(ori_y));
    cJSON_ReplaceItemInObject(orientation, "z", cJSON_CreateNumber(ori_z));
    cJSON_ReplaceItemInObject(orientation, "w", cJSON_CreateNumber(ori_w));
}

void ros_twist_msg(float lin_x, float lin_y, float ang_z) {
    cJSON *linear = cJSON_GetObjectItem(twist_msg, "linear");
    cJSON_ReplaceItemInObject(linear, "x", cJSON_CreateNumber(lin_x));
    cJSON_ReplaceItemInObject(linear, "y", cJSON_CreateNumber(lin_y));

    cJSON *angular = cJSON_GetObjectItem(twist_msg, "angular");
    cJSON_ReplaceItemInObject(angular, "z", cJSON_CreateNumber(ang_z));
}
