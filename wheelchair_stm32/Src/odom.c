/**
  ******************************************************************************
  * @file    Src/odom.c
  * @author  Nicolas Montvernay
  * @version V0.1
  * @date    09/01/2019
  * @brief   Odom configuration and management
  ******************************************************************************
  */

#include <math.h>
#include "main.h"
#include "odom.h"

extern TIM_HandleTypeDef htim2, htim5;
extern Odom_Feedback_t odom_feedback;
extern uint8_t odom_dt_coeff;

#define DT ((float)TASK_1 / 1000.0F)

void Odom_Start(void)
{
	__HAL_TIM_SetCounter(&htim2, MIDDLE_COUNTER);
	__HAL_TIM_SetCounter(&htim5, MIDDLE_COUNTER);

	HAL_TIM_Encoder_Start(&htim2, ENC_LEFT_CHANNEL_1 | ENC_LEFT_CHANNEL_2);
	HAL_TIM_Encoder_Start(&htim5, ENC_RIGHT_CHANNEL_1 | ENC_RIGHT_CHANNEL_2);
}

void Odom_Update(void)
{
	int16_t dR = -1.0F*((float)__HAL_TIM_GET_COUNTER(&htim2) - (float)MIDDLE_COUNTER); // Negative
	int16_t dL = ((float)__HAL_TIM_GET_COUNTER(&htim5) - (float)MIDDLE_COUNTER);

	float d_left_wheel = dL/(ENC_RES * 4.0F);  // (en tr)
	float d_right_wheel = dR/(ENC_RES * 4.0F); // (en tr)
	
	float d_dist_left = d_left_wheel*(WHEEL_DIAMETER * M_PI); // (en m)
	float d_dist_right = d_right_wheel*(WHEEL_DIAMETER * M_PI); // (en m)

	float d_lin = (d_dist_left + d_dist_right) / 2.0F;							// en m
	float d_ang = (d_dist_right - d_dist_left) / WHEEL_TRACK;
	
	float lin_speed = d_lin / (odom_dt_coeff * DT); // en m/s
	float ang_speed = d_ang / (odom_dt_coeff * DT); // en rad/s

	odom_feedback.vangle = ang_speed;
	odom_feedback.vx = lin_speed * cos(d_ang);
	odom_feedback.vy = lin_speed * sin(d_ang);
	
	odom_feedback.x += d_lin * cos(odom_feedback.angle);
	odom_feedback.y += d_lin * sin(odom_feedback.angle);
	odom_feedback.angle = fmod(odom_feedback.angle + d_ang, 2.0F * M_PI);

	// Reset
	__HAL_TIM_SetCounter(&htim5, MIDDLE_COUNTER);
	__HAL_TIM_SetCounter(&htim2, MIDDLE_COUNTER);
	odom_dt_coeff = 0;
}

