#ifndef __COM_H
#define __COM_H

#include "stm32f4xx_hal.h"

typedef struct {
	float vx;
	float vy;
	float vangle;
	float x;
	float y;
	float angle;
	
	float lin_speed;
} Odom_Feedback_t;

typedef struct {
	float x;
	float y;
	float z;
} Vec3_t;

typedef struct {
	Vec3_t angular;
	Vec3_t linear;
} Twist_t;


void Com_Init(void);
void Send_Odom(void);

#endif
