#ifndef __PID_H
#define __PID_H

typedef struct {
	float Kp;
	float Kd;
	float Ki;
	float integ_err;
	float prev_err;
	float wind_up;
	float error_stop;
} PID_t;

float Execute_PID(PID_t *pid, float error);

#endif
