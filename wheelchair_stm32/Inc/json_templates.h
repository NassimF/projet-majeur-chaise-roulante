/**
  ******************************************************************************
  * @file    Inc/json_templates.h
  * @author  Nicolas Montvernay
  * @version V0.1
  * @date    08/01/2019
  * @brief   Json templates for ros messages to send
  ******************************************************************************
  */


char pose_template[] = "{\"position\": {\"x\": 0,\"y\": 0,\"z\": 0},\"orientation\": {\"x\": 0,\"y\": 0,\"z\": 0,\"w\": 0}}";

char twist_template[] = "{\"linear\": {\"x\": 0,\"y\": 0,\"z\": 0},\"angular\": {\"x\": 0,\"y\": 0,\"z\": 0}}";

char odom_template[] = "{\"header\" : {\"seq\": 0,\"stamp\": {\"sec\": 0,\"nsec\": 0},\"frame_id\": \"/odom\"},\"child_frame_id\": \"/map\",\"pose\": {\"pose\": {},\"covariance\": [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]},\"twist\": {\"twist\": {},\"covariance\": [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]}}";
