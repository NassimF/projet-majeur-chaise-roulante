#ifndef __ODOM_H
#define __ODOM_H

// timer 5
#define ENC_LEFT_CHANNEL_1 TIM_CHANNEL_1
#define ENC_LEFT_GPIO_1 GPIOA            // A0
#define ENC_LEFT_PIN_1 GPIO_PIN_0
#define ENC_LEFT_CHANNEL_2 TIM_CHANNEL_2
#define ENC_LEFT_GPIO_2 GPIOA            // A1
#define ENC_LEFT_PIN_2 GPIO_PIN_1

// timer 2
#define ENC_RIGHT_CHANNEL_1 TIM_CHANNEL_1
#define ENC_RIGHT_GPIO_1 GPIOA            // A15
#define ENC_RIGHT_PIN_1 GPIO_PIN_15
#define ENC_RIGHT_CHANNEL_2 TIM_CHANNEL_2
#define ENC_RIGHT_GPIO_2 GPIOB            // B3
#define ENC_RIGHT_PIN_2 GPIO_PIN_3

#define WHEEL_DIAMETER 0.09F // in meter
#define ENC_RES 1024.0F
#define WHEEL_TRACK 0.44F // Distance between wheels (in meter)

#define MIDDLE_COUNTER 32767

#include "com.h"


void Odom_Start(void);
void Odom_Update(void);


#endif
