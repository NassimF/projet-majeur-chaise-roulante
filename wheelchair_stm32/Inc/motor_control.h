#ifndef __MOTOR_CONTROL_H
#define __MOTOR_CONTROL_H

// timer 4
#define MOTOR_LEFT_CHANNEL TIM_CHANNEL_1
#define MOTOR_LEFT_DIR_GPIO GPIOA
#define MOTOR_LEFT_DIR_PIN GPIO_PIN_9
#define MOTOR_LEFT_SLEEP_GPIO GPIOB
#define MOTOR_LEFT_SLEEP_PIN GPIO_PIN_5
#define MOTOR_LEFT_RELAY_GPIO GPIOC
#define MOTOR_LEFT_RELAY_PIN GPIO_PIN_15

// timer 3
#define MOTOR_RIGHT_CHANNEL TIM_CHANNEL_2
#define MOTOR_RIGHT_DIR_GPIO GPIOA
#define MOTOR_RIGHT_DIR_PIN GPIO_PIN_8
#define MOTOR_RIGHT_SLEEP_GPIO GPIOA
#define MOTOR_RIGHT_SLEEP_PIN GPIO_PIN_10
#define MOTOR_RIGHT_RELAY_GPIO GPIOC
#define MOTOR_RIGHT_RELAY_PIN GPIO_PIN_14

#define STOP_MOTORS_DELAY 2000 // in ms

#include "com.h"

void Update_Speed(void);
void Check_Motors_Breaks(void);
void Motor_Set_Left_Speed(float speed);
void Motor_Set_Right_Speed(float speed);
#endif
