# **Logbook**

- <span style="color:blue">Date : 03/01/2022 between 8H <-> 12H</span>

- ***AMIRAT Hani :*** Prise en main du projet 
- ***ENGEL Etienne :*** Prise en main du projet 
- ***FOUADH Nassim :*** Prise en main du projet 
- ***FLEITH William :*** Prise en main du projet



- <span style="color:blue">Date : 03/01/2022 between 13H30 <-> 17H45</span>

- ***AMIRAT Hani :*** Prise en main du site web
- ***ENGEL Etienne :***  Mise en route de la chaise roulante dans son cas basique
- ***FOUADH Nassim :***  Début de création de la box pour la Jetson XAVIER
- ***FLEITH William :*** Etude de la datasheet de l'IMU IMU300RI + prise en main du projet



- <span style="color:blue">Date : 04/01/2022 between 8H <-> 12H</span>

- ***AMIRAT Hani :***  Prise en main de la caméra 3D
- ***ENGEL Etienne :***  Etude des communications entre noeuds
- ***FOUADH Nassim :***  Avancement sur design de la box (état d'avancement à peu près 75%) 
- ***FLEITH William :*** Mise en route de la Jetson Xavier



- <span style="color:blue">Date : 04/01/2022 between 13H30 <-> 17H45</span>

- ***AMIRAT Hani :*** Test du package ROS Object Analytics pour détection d'objets

- ***ENGEL Etienne :*** Passage d'un site web en connection wifi vers une connection localhost

- ***FOUADH Nassim :***  Avancement sur design de la box (état d'avancement à peu près 97%) 
- ***FLEITH William :*** Ergonomie du boitier d'électronique



- <span style="color:blue">Date : 05/01/2022 between 8H <-> 12H</span>

- ***AMIRAT Hani :*** Mis en place d'un code faisant la détection d'objet et estimation de distance
- ***ENGEL Etienne :*** Site web en localhost, il reste la page set_pid
- ***FOUADH Nassim :*** Finition box 100%
- ***FLEITH William :*** Prise en main IMU300RI



- <span style="color:blue">Date : 05/01/2022 between 13H30 <-> 17H45</span>

- ***AMIRAT Hani :*** Recherche sur la fusion de données entre Lidar et caméra 3D
- ***ENGEL Etienne :*** Passage du site en localhost fini
- ***FOUADH Nassim :*** Modification box
- ***FLEITH William :*** connection IMU300RI + début bus CAN



- <span style="color:blue">Date : 06/01/2022 between 8H <-> 12H</span>

- ***AMIRAT Hani :*** Absent
- ***ENGEL Etienne :*** Implémentation communication CAN entre IMU et PC + Etude de l'état actuel de l'odométrie
- ***FOUADH Nassim :*** Début de recherche sur l'interfacage tablette/Jetson
- ***FLEITH William :*** Recherches sur les kalman filter Odometry et données reçues par OpenIMU




- <span style="color:blue">Date : 07/01/2022 between 8H <-> 12H</span>

- ***AMIRAT Hani :*** Test fusion de données entre Lidar et caméra 3D
- ***ENGEL Etienne :*** Implémentation communication CAN entre IMU et PC
- ***FOUADH Nassim :*** Finition box
- ***FLEITH William :*** Recherches sur les kalman filter Odometry et données reçues par OpenIMU



- <span style="color:blue">Date : 07/01/2022 between 13H30 <-> 17H45</span>

- ***AMIRAT Hani :*** Interfaçage Tablette/Jetson
- ***ENGEL Etienne :*** Implémentation communication CAN entre IMU et PC
- ***FOUADH Nassim :*** Lancement de l'impression/ recherche interfacage jetson et tablette 
- ***FLEITH William :*** Etude EKF de Aceinna openIMU



- <span style="color:blue">Date : 10/01/2022 between 8H <-> 12H</span>

- ***AMIRAT Hani :*** Interfacage Tablette/Jetson
- ***ENGEL Etienne :*** Communication établie. Envoie des données vers un Kalman Filter.
- ***FOUADH Nassim :*** Interfacage Tablette/Jetson
- ***FLEITH William :*** Etude EKF de Aceinna openIMU



- <span style="color:blue">Date : 10/01/2022 between 13H30 <-> 17H45</span>

- ***AMIRAT Hani :*** Interfacage Tablette/Jetson
- ***ENGEL Etienne :*** Conversion des données de l'IMU vers le KF
- ***FOUADH Nassim :***  Interfacage Tablette/Jetson fait- reste à encapsuler le tout dans un bashrc - Test box 3D sur la jetson
- ***FLEITH William :*** Capteur sharp numérique

- <span style="color:blue">Date : 11/01/2022 between 8H <-> 12H</span>

- ***AMIRAT Hani :*** Création application avec MIT App Inventor
- ***ENGEL Etienne :*** Calibration de l'IMU
- ***FOUADH Nassim :*** Avancement sur l'interface tablette/Jetson
- ***FLEITH William :*** placement IMU et Jetson sur chaise

- <span style="color:blue">Date : 11/01/2022 between 13H30 <-> 17H45</span>

- ***AMIRAT Hani :*** Création application avec MIT App Inventor
- ***ENGEL Etienne :*** Calibration de l'IMU
- ***FOUADH Nassim :*** Test et installation de l'IMU sur la Jetson
- ***FLEITH William :*** idem matin + recherches sur IMU

- <span style="color:blue">Date : 12/01/2022 between 8H <-> 12H</span>

- ***AMIRAT Hani :*** Build application pour wifi avec kivy
- ***ENGEL Etienne :*** Abscence rattrapée jeudi
- ***FOUADH Nassim :*** test implementation bus can Jetson = FAILED
- ***FLEITH William :*** implementation nouveau boitier sur chaise roulante

- <span style="color:blue">Date : 12/01/2022 between 13H30 <-> 17H45</span>

- ***AMIRAT Hani :*** Build application pour wifi avec kivy
- ***ENGEL Etienne :*** Conversion des données de l'IMU
- ***FOUADH Nassim :*** test implementation bus can sur board soudé + FAILED / Redaction script bash pour creation d'un hotspot de manière automatique au boot
- ***FLEITH William :*** changement des composants de boitier + alimentation Jetson Xavier

- <span style="color:blue">Date : 13/01/2022 between 8H <-> 12H</span>

- ***AMIRAT Hani :*** Build application pour wifi avec kivy
- ***ENGEL Etienne :*** Conversion des données de l'IMU + MAJ du linux de la Jetson
- ***FOUADH Nassim :*** Successful integration of wheelchair project on updated wheelchair
- ***FLEITH William :*** Test wheelchair fonctionnel

- <span style="color:blue">Date : 14/01/2022 between 8H <-> 12H</span>

- ***AMIRAT Hani :*** Application wifi avec kivy (Debugage des erreurs)
- ***ENGEL Etienne :*** MAJ du repo gitlab + Conversion données IMU
- ***FOUADH Nassim :*** Aide de debogage appli avec ramzy
- ***FLEITH William :*** Cable management boitier wheelchair

- <span style="color:blue">Date : 14/01/2022 between 13H30 <-> 17H45</span>

- ***AMIRAT Hani :*** Développement de wheelchair app sur android studio
- ***ENGEL Etienne :*** L'accéléromètre de l'IMU fonctionne. Intégration du package robot_pose_ekf
- ***FOUADH Nassim :*** 
- ***FLEITH William :*** 

- <span style="color:blue">Date : 17/01/2022 between 8H <-> 12H</span>

- ***AMIRAT Hani :*** Développement de wheelchair app sur android studio
- ***ENGEL Etienne :*** Intégration package robot_pose_ekf
- ***FOUADH Nassim :*** 
- ***FLEITH William :*** 

- <span style="color:blue">Date : 17/01/2022 between 13H30 <-> 17H45</span>

- ***AMIRAT Hani :*** Développement de wheelchair app sur android studio
- ***ENGEL Etienne :*** Package robot_pose_ekf publish rien en simu, essai sur chaise obligatoire
- ***FOUADH Nassim :*** 
- ***FLEITH William :*** 

- <span style="color:blue">Date : 18/01/2022 between 8H <-> 12H</span>

- ***AMIRAT Hani :*** 
- ***ENGEL Etienne :*** Essais sur chaise
- ***FOUADH Nassim :*** Essais sur chaise et debugs
- ***FLEITH William :*** 

- <span style="color:blue">Date : 18/01/2022 between 13H30 <-> 17H45</span>

- ***AMIRAT Hani :*** 
- ***ENGEL Etienne :*** 
- ***FOUADH Nassim :*** Essais sur chaise et debugs
- ***FLEITH William :*** 
