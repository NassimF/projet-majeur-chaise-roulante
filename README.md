# CPE WHEELCHAIR



## Table of Content

1. [About the project](#about-the-project)
1. [Installation](#installation)
1. [Usage](#usage)
1. [Roadmap](#roadmap)
1. [Authors and acknowledgment](#authors-and-acknowledgment)
1. [License](#license)
1. [Sources](#sources)



## About the project

### Context

This repository aims to develop an autonomous wheelchair.

In a known map, send to the wheelchair an initial position, a goal coordinates and change PID parameters thanks to a website.
The wheelchair will move autonomously to the destination.

### Visuals

A software overview, a hardware overview and a video of the final version of the robot are available in the `docs` folder.

![](docs/wheelchair_communications_overview.png)

![](docs/hardware_overview.png)

The Youtube link for the video is: [https://youtu.be/vKPlYHdMC3U]



## Installation

### Hardware

General:

1. Wheelchair
1. Stop button
1. Nucleo STM32F401RE
1. Jetson Xavier AGX
1. 2 wifi antenna linked to the Jetson

Sensors:

1. 2 LIDAR sensors
1. 2 encoding wheels
1. 1 9-DOF IMU (accelerometer, gyroscope and magnetometer)

Powersupply:

1. Regulator (24V to 5V) linked to 2 lipos batteries in series
1. Boost 12V to 19V linked to a single lipo battery to power the Jetson
1. 3 lipos battery 11.1V - 6500mAh
    1. The battery voltage must not fall below 4V (individually)
1. H bridge

Interfacing:

1. FTDI
1. USB hub
1. Handmade connectivity board

### Software

List of software versions on the Jetson Xavier:

1. Ubuntu MATE 18.04
1. ROS melodic
1. Python 2.7.17

List of major frameworks/libraries on the Jetson Xavier:

1. ROS server depends to be able to use ros directly in the javascript (ned to retrieve shell command results),
```
sudo apt-get install ros-melodic-roswww
```
2. [Create a linux wifi hotspot](https://github.com/lakinduakash/linux-wifi-hotspot),
```
git clone https://github.com/lakinduakash/linux-wifi-hotspot
cd linux-wifi-hotspot/src/scripts
make install
create_ap wlan0 eth0 wheelchair_wifi wheelchaircpe
```
3. Use the ROS navigation stack
```
sudo apt-get install ros-melodic-navigation
```


Enable specific behaviours on the Jetson Xavier:

1. Automatically start an application when the Jetson Xavier boot using the [robot_upstart](https://roboticsbackend.com/make-ros-launch-start-on-boot-with-robot_upstart/) package


The `create_hotspot.service` service has been created on the `/etc/systemd/system` folder to create the hotspot on each boot through the `/create_hotspot.sh`.
The ROS robot_upstart package has been used to launch the `wheelchair_start.launch` file on each boot.
```
rosrun robot_upstart install wheelchair_navigation/launch/wheelchair_start.launch --job wheelchair_start --symlink
```



## Usage

### Process

1. Power up the 3 batteries: 1 for Jetson Xavier AJX and 2 for the motors, Nucleo STM32F401RE, ...
1. Wait to see the wifi and connect to it: name=`wheelchair_wifi`, pwd=`wheelchaircpe`
1. Connect to the IP Address `192.168.12.1:5000`
1. Fill in an initial position
1. Fill in a final position

### Features

Features:
1. ROS Navigation Stack
1. Website
1. Odometry with encoding wheels
1. LIDAR point cloud
1. PID to control the 2 motors
1. Manually control the Jetson Xavier fan:
Move to the following directory and set a pwm value between 0 (no rotation) and 255 (max speed rotation), this value will be reset when the Jetson is shutdown:
```
cd /sys/devices/pwm-fan
echo 255 | sudo tee target_pwm
```

### Content

This repository has 4 folders:
1. The `docs` folder contains
    1. 1 zip of the stm32 files in the USB embedded on the wheelchair
    1. 1 `logbook.md` containing a description of our personal work each 4h
    1. General documentation files
1. The `3D` folder contains the files used to 3D printing
1. The `ros` folder contains the ROS packages used on the embedded linux
1. The `wheelchair_stm32` folder containing a version of the stm32 code embedded (like the zip in the `docs` folder)

This repository has 6 other branches than `master`:
1. `dev`
    1. The purpose is to have a stability test before merging on `master`
1. `dev_camera_3d`
    1. The purpose is to have a better point cloud of the environment by adding a camera's point cloud to the LIDAR's point cloud
    1. More information on its [README](https://gitlab.com/5eti_prj_21-22/Sujet_2__Chaise_roulante_autonome/S2_G5_Engel_Amirat_Fouadh_Fleith/-/tree/dev_camera_3d/)
1. `dev_imu`
    1. The purpose is to have a better odometry by adding a Extended Kalman Filter merging an IMU sensor msg and an Odometry msg from the encoding wheels
    1. More information on its [README](https://gitlab.com/5eti_prj_21-22/Sujet_2__Chaise_roulante_autonome/S2_G5_Engel_Amirat_Fouadh_Fleith/-/tree/dev_imu/ros/src/wheelchair_imu/src) 
1. `dev_sharp`
    1. The purpose is to detect if or not a person is on the wheelchair and set the motors's PID consequently
    1. More information on its [README](https://gitlab.com/5eti_prj_21-22/Sujet_2__Chaise_roulante_autonome/S2_G5_Engel_Amirat_Fouadh_Fleith/-/tree/dev_sharp/sharp_ws/src/sharp_sensor_data)
1. `dev_app`
    1. The purpose is to automatically connect a client device to the wheelchair server and open the IP address of the website
    1. More information on its [README](https://gitlab.com/5eti_prj_21-22/Sujet_2__Chaise_roulante_autonome/S2_G5_Engel_Amirat_Fouadh_Fleith/-/tree/dev_app)



## Roadmap
Ideas for future releases:
1. Check sensors's data and check actuator's response
1. Check the USB communication between the Nucleo STM32F401RE and the Jetson Xavier AGX ![](docs/comSTM32_error.png)
1. Integrate each feature on the system. Each feature has its branch and its README
    1. Implement the IMU to upgrade the odometry
    1. Implement an application to directly connect the client to the server or connect a device to the Jetson with a localhost website
    1. Implement a sharp sensor to detect if someone is on the wheelchair and update PID params
1. Upgrade the ROS navigation stack's local planner



## Authors and acknowledgment

1. AMIRAT Hani
1. ENGEL Etienne
1. FLEITH William
1. FOUADH Nassim



## License

Distributed under the MIT License.



## Sources

- [ ] Realsense camera documentation : 

https://dev.intelrealsense.com/docs/python2

https://github.com/Mazhichaoruya/ROS-Object-Detection-2Dto3D-RealsenseD435
